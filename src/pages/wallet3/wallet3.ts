import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Wallet3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wallet3',
  templateUrl: 'wallet3.html',
})
export class Wallet3Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Wallet3Page');
  }

  back(){
    this.navCtrl.pop();
  }

  next(){
    this.navCtrl.push("Wallet4Page");
  }

}
