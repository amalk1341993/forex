import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Wallet3Page } from './wallet3';

@NgModule({
  declarations: [
    Wallet3Page,
  ],
  imports: [
    IonicPageModule.forChild(Wallet3Page),
  ],
})
export class Wallet3PageModule {}
