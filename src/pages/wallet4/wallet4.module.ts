import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Wallet4Page } from './wallet4';

@NgModule({
  declarations: [
    Wallet4Page,
  ],
  imports: [
    IonicPageModule.forChild(Wallet4Page),
  ],
})
export class Wallet4PageModule {}
