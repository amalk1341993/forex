import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Wallet2Page } from './wallet2';

@NgModule({
  declarations: [
    Wallet2Page,
  ],
  imports: [
    IonicPageModule.forChild(Wallet2Page),
  ],
})
export class Wallet2PageModule {}
