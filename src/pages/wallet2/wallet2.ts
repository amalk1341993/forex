import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Wallet2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wallet2',
  templateUrl: 'wallet2.html',
})
export class Wallet2Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Wallet2Page');
  }

  back(){
    this.navCtrl.pop();
  }

  next(){
    this.navCtrl.push("Wallet3Page");
  }

}
