import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Wallet1Page } from './wallet1';

@NgModule({
  declarations: [
    Wallet1Page,
  ],
  imports: [
    IonicPageModule.forChild(Wallet1Page),
  ],
})
export class Wallet1PageModule {}
