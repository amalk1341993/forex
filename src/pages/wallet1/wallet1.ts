import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Wallet1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wallet1',
  templateUrl: 'wallet1.html',
})
export class Wallet1Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Wallet1Page');
  }

  back()
  {
    this.navCtrl.pop();
  }
  next(){
    this.navCtrl.push("Wallet2Page");
  }

}
