import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabmainPage } from './tabmain';

@NgModule({
  declarations: [
    TabmainPage,
  ],
  imports: [
    IonicPageModule.forChild(TabmainPage),
  ],
})
export class TabmainPageModule {}
