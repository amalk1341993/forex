import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TabmainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabmain',
  templateUrl: 'tabmain.html',
})
export class TabmainPage {

  tab1Root = 'AccountPage';
  tab2Root = 'TransactionPage';
  tab3Root = 'CardPage';
  tab4Root = 'MorePage';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabmainPage');
  }

}
